﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace JLib.Media
{
    public class Imager
    {
        /// <summary>
        /// 图片格式转换
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="distationPath"></param>
        /// <param name="format"></param>
        public static void ImageFormatter(string sourcePath, string distationPath, string format)
        {
            System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(sourcePath);
            switch (format.ToLower())
            {
                case "bmp":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Bmp);
                    break;
                case "emf":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Emf);
                    break;
                case "gif":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Gif);
                    break;
                case "ico":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Icon);
                    break;
                case "jpg":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                    bitmap.Dispose();
                    break;
                case "png":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Png);
                    break;
                case "tif":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Tiff);
                    break;
                case "wmf":
                    bitmap.Save(distationPath, System.Drawing.Imaging.ImageFormat.Wmf);
                    break;
                default: throw new Exception("无法转换此格式！");
            }
        }

        /// <summary>
        /// 图片压缩
        /// </summary>
        /// <param name="frompath">源图</param>
        /// <param name="topath">目标地址</param>
        /// <param name="height">压缩宽度:默认原宽度</param>
        /// <param name="width">压缩高度:默认原高度</param>
        public static void ImageCompress(string frompath, string topath, int width = -1, int height = -1)
        {
            Image img;
            Bitmap bmp;
            Graphics grap;
            try
            {
                img = Image.FromFile(frompath);
                width = width == -1 ? width = Convert.ToInt32(img.Width) : width;
                height = height == -1 ? Convert.ToInt32(img.Height) : height;

                bmp = new Bitmap(width, height);
                grap = Graphics.FromImage(bmp);
                grap.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                grap.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                grap.DrawImage(img, new Rectangle(0, 0, width, height));

                bmp.Save(topath, System.Drawing.Imaging.ImageFormat.Jpeg);
                bmp.Dispose();
                img.Dispose();
                grap.Dispose();
            }
            catch (Exception ex)
            {
                return;
            }
        }


        /// <summary>
        /// 图片增加边框
        /// </summary>
        /// <param name="sourcePath">源图片</param>
        /// <param name="distationPath">目的图片</param>
        /// <param name="width">边框宽度</param>
        /// <param name="color">边框颜色(Ex:Color.White)</param>
        /// <param name="style">边框类型：默认0(0:全边框 1.上下边框 2:左右边框 )</param>
        public static void AddBorder(string sourcePath, string distationPath, int width, Color color, int style = 0)
        {
            using (Image img = Bitmap.FromFile(sourcePath))
            {
                int newheight = img.Height + width*2;
                int newwidth = img.Width + width*2;
                Color bordcolor = color;

                Bitmap bmp = new Bitmap(newwidth, newheight);
                Graphics g = Graphics.FromImage(bmp);

                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

                switch (style)
                {
                    case 0://New: 整个边框.
                        {
                            System.Drawing.Rectangle rec = new Rectangle(0, 0, newwidth - width/2, newheight - width/2);
                            g.DrawImage(img, rec, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                            g.DrawRectangle(new Pen(bordcolor, width), 0, 0, newwidth, newheight);
                            break;
                        }
                    case 1://New: 上下边框.
                        {
                            System.Drawing.Rectangle rec = new Rectangle(0, width / 2, newwidth, newheight - width / 2);
                            g.DrawImage(img, rec, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                            g.DrawLine(new Pen(bordcolor, width), 0, 0, newwidth, 0);
                            g.DrawLine(new Pen(bordcolor, width), 0, newheight, newwidth, newheight);
                            break;
                        }
                    case 2://New: 左右边框.
                        {
                            System.Drawing.Rectangle rec = new Rectangle(width / 2, 0, newwidth - width / 2, newheight);
                            g.DrawImage(img, rec, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);
                            g.DrawLine(new Pen(bordcolor, width), 0, 0, 0, newheight);
                            g.DrawLine(new Pen(bordcolor, width), newwidth, 0, newwidth, newheight);
                            break;
                        }
                }

                bmp.Save(distationPath);
                g.Dispose();
            }

        }


    }
}
